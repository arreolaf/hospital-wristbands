/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hospital;

/**
 *
 * @author mymac
 */
public class WristBands {
    private String barcode;
    private String information;
    
    WristBands(String barcode, String information){
        this.barcode = barcode;
        this.information = information;
    }
    
    public String getBarcode(){
        return barcode;
    }
    
    public String getInformation(){
        return information;
    }
}
