/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hospital;

/**
 *
 * @author mymac
 */
public class Patients {
    private String name;
    private String dateOfBirth;
    private String familyDoctor;
    private WristBands wristBand;
    
    Patients (String name,String dateOfBirth, String familyDoctor, WristBands wristBand){
        this.name = name;
        this.dateOfBirth = dateOfBirth;
        this.familyDoctor = familyDoctor;
        this.wristBand = wristBand;
    }
    public String getWristBand(){
        return wristBand.getBarcode();
    }
    public String getWristBandInformation(){
        return wristBand.getInformation();
    }
    
    public String getName(){
        return name ;
    }
    
    public String getDateOfBirth(){
        return dateOfBirth ;
    }

    public String getFamilyDoctor(){
        return familyDoctor ;
    }

    
}
