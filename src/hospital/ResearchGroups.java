/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hospital;

/**
 *
 * @author mymac
 */
public class ResearchGroups {
    private int timeWaited;
    private int patientsGroup;
    
    
    ResearchGroups (int timeWaited, int patientsGroup){
        this.timeWaited = timeWaited;
        this.patientsGroup = patientsGroup;
    }
    
    public int getTimeWaited(){
        return timeWaited;
        
    }
    
    public int getPatientsGroup(){
        return patientsGroup;
    }
    
}
